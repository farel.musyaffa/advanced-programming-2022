package id.ac.ui.cs.advprog.tutorial0.exception;

public class DuplicateCourseException extends RuntimeException {

    public DuplicateCourseException(String courseName) {
        super(String.format("The course %s already exists", courseName));
    }
}
